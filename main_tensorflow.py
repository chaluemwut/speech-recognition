import numpy as np
import keras, pickle
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv1D, MaxPooling1D
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
import librosa, keras, random



def find_max_time():
    max_time = -1

    for i in range(31):
        x, rate = librosa.load('data/training/cat/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = len(mfcc[0])
        if mfcc_time > max_time:
            max_time = mfcc_time

    for i in range(31):
        x, rate = librosa.load('data/training/dog/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = len(mfcc[0])
        if mfcc_time > max_time:
            max_time = mfcc_time
    return max_time

def create_feature():
    max_time = find_max_time()
    x_feature = []
    y = []
    for i in range(31):
        x, rate = librosa.load('data/training/cat/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = mfcc[0].size
        new_data = np.pad(mfcc, ((0, 0), (0, max_time - mfcc_time)), 'constant')
        x_feature.append(new_data)
        y.append(1)

    for i in range(31):
        x, rate = librosa.load('data/training/dog/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = mfcc[0].size
        new_data = np.pad(mfcc, ((0, 0), (0, max_time - mfcc_time)), 'constant')
        x_feature.append(new_data)
        y.append(0)

    return x_feature, np.array(y)

# x_train, y_train = create_feature()
#
# x_train = np.array(x_train).reshape((62, 40, 299))

# y_train = keras.utils.to_categorical(np.random.randint(10, size=(62, 1)), num_classes=2)

# Generate dummy data
# x_train_1 = np.random.random((400, 40, 299))
# y_train_1 = keras.utils.to_categorical(np.random.randint(10, size=(400, 1)), num_classes=10)
# x_test = np.random.random((20, 27, 27))
# y_test = keras.utils.to_categorical(np.random.randint(10, size=(20, 1)), num_classes=10)

x_train, y_train = pickle.load(open('data/obj/x_train.obj', 'rb')), pickle.load(open('data/obj/y_train.obj', 'rb'))

model = Sequential()
# input: 100x100 images with 3 channels -> (100, 100, 3) tensors.
# this applies 32 convolution filters of size 3x3 each.
model.add(Conv1D(32, 3, activation='relu', input_shape=(40, 299)))
model.add(Conv1D(32, 3, activation='relu'))
model.add(MaxPooling1D(3))
model.add(Dropout(0.25))

model.add(Conv1D(64, 3, activation='relu'))
model.add(Conv1D(64, 3, activation='relu'))
model.add(MaxPooling1D(3))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(1, activation='softmax'))

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss=keras.losses.binary_crossentropy, optimizer=sgd)

model.fit(x_train, y_train, batch_size=32, epochs=10)
score = model.predict(x_train[0:10])
print(score)