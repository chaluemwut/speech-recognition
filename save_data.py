import numpy as np
import keras, pickle
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv1D, MaxPooling1D
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
import librosa, keras, random

def find_max_time():
    max_time = -1

    for i in range(31):
        x, rate = librosa.load('data/training/cat/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = len(mfcc[0])
        if mfcc_time > max_time:
            max_time = mfcc_time

    for i in range(31):
        x, rate = librosa.load('data/training/dog/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = len(mfcc[0])
        if mfcc_time > max_time:
            max_time = mfcc_time
    return max_time

def create_feature():
    max_time = find_max_time()
    x_feature = []
    y = []
    for i in range(31):
        x, rate = librosa.load('data/training/cat/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = mfcc[0].size
        new_data = np.pad(mfcc, ((0, 0), (0, max_time - mfcc_time)), 'constant')
        x_feature.append(new_data)
        y.append(1)

    for i in range(31):
        x, rate = librosa.load('data/training/dog/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        mfcc_time = mfcc[0].size
        new_data = np.pad(mfcc, ((0, 0), (0, max_time - mfcc_time)), 'constant')
        x_feature.append(new_data)
        y.append(0)

    return x_feature, np.array(y)

x_train, y_train = create_feature()

x_train = np.array(x_train).reshape((62, 40, 299))

pickle.dump(x_train, open('data/obj/x_train.obj', 'wb'))
pickle.dump(y_train, open('data/obj/y_train.obj', 'wb'))
