import librosa, keras, random
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score

def create_feature():
    x_feature = []
    y = []
    for i in range(31):
        x, rate = librosa.load('data/training/cat/{}.wav'.format(i))
        mfcc = np.mean(librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40).T, axis=0)
        x_feature.append(mfcc)
        y.append(1)

    for i in range(31):
        x, rate = librosa.load('data/training/dog/{}.wav'.format(i))
        mfcc = np.mean(librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40).T, axis=0)
        x_feature.append(mfcc)
        y.append(0)

    return np.array(x_feature), np.array(y)


def process():
    x, y = create_feature()
    # Train the model, iterating on the data in batches of 32 samples
    scores_list = []
    for i in range(100):
        print("********************* loop ************* {}".format(i))
        model = Sequential()
        model.add(Dense(64, input_dim=40, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1, activation='sigmoid'))

        model.compile(loss='binary_crossentropy',
                      optimizer='rmsprop')
        x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.3, random_state=int(random.uniform(1,1000)))
        model.fit(x_train, y_train, epochs=10, batch_size=32)
        y_pred = model.predict(x_test).astype(int)
        y_pred = y_pred.reshape((1,-1))[0]
        print(y_pred)
        scores = f1_score(y_test, y_pred)
        scores_list.append((scores*100))

    # import matplotlib.pyplot as plt
    # plt.plot(scores_list)
    # plt.show()
    # print(scores_list)
    # print(np.average(scores_list))

if __name__ == '__main__':
    process()