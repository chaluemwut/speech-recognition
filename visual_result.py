from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv1D, MaxPooling1D

def create_model():
    model = Sequential()
    # input: 100x100 images with 3 channels -> (100, 100, 3) tensors.
    # this applies 32 convolution filters of size 3x3 each.
    model.add(Conv1D(32, 3, activation='relu', input_shape=(40, 299)))
    model.add(Conv1D(32, 3, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.25))

    model.add(Conv1D(64, 3, activation='relu'))
    model.add(Conv1D(64, 3, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='softmax'))
    return model


def visual_model(model):
    # from keras.utils import plot_model
    # plot_model(model, to_file='model.png')
    from keras_sequential_ascii import keras2ascii
    keras2ascii(model)

if __name__ == '__main__':
    visual_model(create_model())

    # import matplotlib.pyplot as plt
    # import pickle
    # import numpy as np
    # f1 = pickle.load(open('data/result/f1_list.obj', 'rb'))
    # print(np.average(f1)*100)
    # plt.boxplot(f1)
    # plt.show()