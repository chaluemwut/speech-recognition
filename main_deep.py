import pickle, keras, random
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv1D, MaxPooling1D
from keras.optimizers import SGD
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
import numpy as np

def deep_process():
    x, y = pickle.load(open('data/obj/x_train.obj', 'rb')), pickle.load(open('data/obj/y_train.obj', 'rb'))
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=int(random.uniform(1, 1000)))

    model = Sequential()
    # input: 100x100 images with 3 channels -> (100, 100, 3) tensors.
    # this applies 32 convolution filters of size 3x3 each.
    model.add(Conv1D(32, 3, activation='relu', input_shape=(40, 299)))
    model.add(Conv1D(32, 3, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.25))

    # model.add(Conv1D(64, 3, activation='relu'))
    model.add(Conv1D(64, 3, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss=keras.losses.binary_crossentropy, optimizer=sgd)
    model.fit(x_train, y_train, batch_size=32, epochs=10)
    y_pred = model.predict(x_test)
    scores = f1_score(y_test, y_pred)
    return scores

def process():
    f1_list = []
    for i in range(10):
        print("******** loop {}".format(i))
        f1 = deep_process()
        f1_list.append(f1)

    print(np.average(f1_list)*100)
    # pickle.dump(f1_list, open('data/result/f1_list.obj', 'wb'))

if __name__ == '__main__':
    process()