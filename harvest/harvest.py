from pytube import YouTube
import json

def load_json():
    from pprint import pprint
    data = None
    with open('ontology.json') as f:
        data = json.load(f)

    x_name = [x for x in data if x['name'] == 'Cat']
    print(x_name[0]['positive_examples'])
    pprint(x_name)
    # return x_name['positive_examples']

def vdo_download(url):
    import requests
    requests.packages.urllib3.disable_warnings()

    import ssl

    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
        pass
    else:
        # Handle target environment that doesn't support HTTPS verification
        ssl._create_default_https_context = _create_unverified_https_context

    print(YouTube(url).streams.filter(subtype='mp4').all()[0].download('../data/training/raw'))

data = {
    0: 'https://www.youtube.com/watch?time_continue=34&v=-ZJqu_4zLMc',
    1: 'https://www.youtube.com/watch?time_continue=30&v=-gSfPQqi6nI',
    2: 'https://www.youtube.com/watch?time_continue=30&v=-uGHAvfqs2I',
    3: 'https://www.youtube.com/watch?time_continue=10&v=-oxF9jhY94s',
    4: 'https://www.youtube.com/watch?time_continue=35&v=-urYwYLrcMA',
    5: 'https://www.youtube.com/watch?time_continue=25&v=0_utuoBWKmo',
    6: 'https://www.youtube.com/watch?time_continue=26&v=0jFN112eGVQ',
    7: 'https://www.youtube.com/watch?time_continue=40&v=0pJwADdUrRo',
    8: 'https://www.youtube.com/watch?time_continue=37&v=0qbp0OtOvl4',
    9: 'https://www.youtube.com/watch?time_continue=35&v=1G3TdBrW7ys',
    10: 'https://www.youtube.com/watch?time_continue=33&v=1HPYHsdLYCQ',
    11: 'https://www.youtube.com/watch?time_continue=33&v=1lBl4WXrFxE',
    12: 'https://www.youtube.com/watch?time_continue=8&v=29x5I94EBMQ',
    14: 'https://www.youtube.com/watch?time_continue=19&v=2HlCRjjDhIA',
    15: 'https://www.youtube.com/watch?time_continue=40&v=2qJ-efHMACE',
    16: 'https://www.youtube.com/watch?time_continue=40&v=30UUl_YYwpk',
    17: 'https://www.youtube.com/watch?time_continue=445&v=33_enN0Bsfs',
    18: 'https://www.youtube.com/watch?time_continue=34&v=3HaqTs1_Exg',
    19: 'https://www.youtube.com/watch?time_continue=32&v=3hqBv_pF_dw',
    20: 'https://www.youtube.com/watch?time_continue=14&v=3rmsgqvn-7g',
    21: 'https://www.youtube.com/watch?time_continue=16&v=4MZrJBy7i-I',
    22: 'https://www.youtube.com/watch?time_continue=4&v=5F7MMaPJqMc',
    23: 'https://www.youtube.com/watch?time_continue=15&v=5KWTAq3GkfA',
    24: 'https://www.youtube.com/watch?time_continue=40&v=5atdlBjDap0',
    25: 'https://www.youtube.com/watch?time_continue=38&v=5wjiU7kz9rM',
    26: 'https://www.youtube.com/watch?time_continue=34&v=6c-5b9sj35o',
    27: 'https://www.youtube.com/watch?time_continue=38&v=6u14wxqIlZ4',
    28: 'https://www.youtube.com/watch?time_continue=36&v=6wg3GJZk7Q0',
    29: 'https://www.youtube.com/watch?time_continue=40&v=7F01MogWvhQ',
    30: 'https://www.youtube.com/watch?time_continue=21&v=7WoFyN1R-1g',
    31: 'https://www.youtube.com/watch?time_continue=10&v=7fAxjx6m_Ak',
    32: 'https://www.youtube.com/watch?time_continue=40&v=7iyRkyHjsTE',
    33: 'https://www.youtube.com/watch?time_continue=33&v=7m6w6NH5RaY',
    34: 'https://www.youtube.com/watch?time_continue=37&v=82EHZ4_pTps',
    36: 'https://www.youtube.com/watch?time_continue=19&v=BFWN_OXjYM0',
    37: 'https://www.youtube.com/watch?time_continue=37&v=BGkXV9_wf9g',
    38: 'https://www.youtube.com/watch?time_continue=34&v=CxNgd-mdl1A',
    39: 'https://www.youtube.com/watch?time_continue=53&v=DOOUC0UPOKA',
    40: 'https://www.youtube.com/watch?time_continue=33&v=HJFlxLJSX8E',
    41: 'https://www.youtube.com/watch?time_continue=65&v=IOzFIWgL_IQ',
    42: 'https://www.youtube.com/watch?time_continue=35&v=JqRCZKXNXwY',
    43: 'https://www.youtube.com/watch?time_continue=31&v=KVyTY6NAXL8',
    44:'https://www.youtube.com/watch?time_continue=228&v=L6dzsJXYPGI',
    45:'https://www.youtube.com/watch?time_continue=39&v=Qt1aCUq51ns'
}

dog = {
    0: 'https://www.youtube.com/watch?time_continue=11&v=-Ry1YNVPPCM',
    1:'https://www.youtube.com/watch?time_continue=32&v=-qGd1JwYLbQ',
    2: 'https://www.youtube.com/watch?time_continue=36&v=-qJ-NY6wY3s',
    3:'https://www.youtube.com/watch?time_continue=22&v=-skhG36uWM0',
    4:'https://www.youtube.com/watch?time_continue=52&v=06OT6nqAuLM',
    5:'https://www.youtube.com/watch?time_continue=32&v=0rhb_K228gA',
    6:'https://www.youtube.com/watch?time_continue=34&v=1PxKjFulkLs',
    7:'https://www.youtube.com/watch?time_continue=23&v=1eI2-vhs0YY',
    8:'https://www.youtube.com/watch?time_continue=35&v=2p7QjnOHpUI',
    9:'https://www.youtube.com/watch?time_continue=72&v=36BQNuC3SSo',
    10:'https://www.youtube.com/watch?time_continue=52&v=3xCWI_22Z9A',
    11:'https://www.youtube.com/watch?time_continue=33&v=3zEWsAPEdd0',
    12:'https://www.youtube.com/watch?time_continue=32&v=4TgW0mtugns',
    13:'https://www.youtube.com/watch?time_continue=20&v=4sFGGOVcvM8',
    14:'https://www.youtube.com/watch?time_continue=25&v=62oVSUMNL7c',
    15:'https://www.youtube.com/watch?time_continue=20&v=6qmGCfCrU6w',
    16:'https://www.youtube.com/watch?time_continue=34&v=7DFJpPvm36g',
    17:'https://www.youtube.com/watch?time_continue=37&v=7gqGqbzLpZE',
    18:'https://www.youtube.com/watch?time_continue=22&v=7t7RfDh-YKA',
    19:'https://www.youtube.com/watch?time_continue=35&v=ABs2ECDx-EQ',
    20:'https://www.youtube.com/watch?time_continue=3&v=A09VB0vE1IE',
    21:'https://www.youtube.com/watch?time_continue=67&v=BkjvDGA8_94',
    22:'https://www.youtube.com/watch?time_continue=32&v=CCdCRvQzf_c',
    23:'https://www.youtube.com/watch?time_continue=42&v=CMBAs27Sn1w',
    24:'https://www.youtube.com/watch?time_continue=35&v=CONgGb-D4Iw',
    25:'https://www.youtube.com/watch?time_continue=53&v=DIc3zDgrbZg',
    26:'https://www.youtube.com/watch?time_continue=26&v=FeRaDiSPb2c',
    29:'https://www.youtube.com/watch?time_continue=11&v=JusleurtLGs',
    30:'https://www.youtube.com/watch?time_continue=101&v=KrtiLKd4VCI',
    31:'https://www.youtube.com/watch?time_continue=1&v=RB9xMnuB59A'
}

if __name__ == '__main__':
    vdo_download(dog[31])