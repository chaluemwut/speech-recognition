import librosa
import numpy as np

def process():
    print("hello world")
    x, rate = librosa.load('data/training/cat/{}.wav'.format(24))
    mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
    print(mfcc)

if __name__ == '__main__':
    process()