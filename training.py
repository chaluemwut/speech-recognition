import librosa, keras
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from sklearn import metrics

# https://www.analyticsvidhya.com/blog/2017/08/audio-voice-processing-deep-learning/
# https://keras.io/getting-started/sequential-model-guide/
# https://research.google.com/audioset/balanced_train/cat.html

def build(x, y):
    model = Sequential()
    model.compile(optimizer=keras.optimizers.Adam(), loss='binary_crossentropy', metrics=['accuracy'])

    model.fit(x, y)

    return model

def create_feature():
    num_training = 4
    x_feature = []
    for i in range(num_training):
        x, rate = librosa.load('data/training/{}.wav'.format(i))
        mfcc = np.mean(librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40).T, axis=0)
        x_feature.append(mfcc)

    return np.array(x_feature), np.ones(num_training)


def model2():
    # https://machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras/
    import numpy
    from keras.datasets import imdb
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import LSTM
    from keras.layers.embeddings import Embedding
    from keras.preprocessing import sequence
    # fix random seed for reproducibility
    numpy.random.seed(7)
    import ssl

    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
        pass
    else:
        # Handle target environment that doesn't support HTTPS verification
        ssl._create_default_https_context = _create_unverified_https_context

    top_words = 5000
    (X_train, y_train), (X_test, y_test) = imdb.load_data(num_words=top_words)
    max_review_length = 500
    X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
    X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)
    embedding_vecor_length = 32

    model = Sequential()
    model.add(Embedding(top_words, embedding_vecor_length, input_length=max_review_length))
    model.add(LSTM(100))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=3, batch_size=64)
    scores = model.evaluate(X_test, y_test, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1]*100))

def model3():
    model = Sequential()
    model.add(Dense(32, activation='relu', input_dim=100))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(optimizer='rmsprop',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])

    # Generate dummy data
    import numpy as np
    data = np.random.random((1000, 100))
    labels = np.random.randint(2, size=(1000, 1))

    # Train the model, iterating on the data in batches of 32 samples
    model.fit(data, labels, epochs=10, batch_size=32)

def model4():
    # model = Sequential()
    # model.add(Dense(32, activation='relu', input_dim=40))
    # model.add(Dense(1, activation='sigmoid'))
    # model.compile(optimizer='rmsprop',
    #               loss='binary_crossentropy',
    #               metrics=['accuracy'])

    model = Sequential()
    model.add(Dense(64, input_dim=40, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    # Generate dummy data
    # import numpy as np
    # data = np.random.random((1000, 100))
    # labels = np.random.randint(2, size=(1000, 1))

    data, labels = create_feature()
    # Train the model, iterating on the data in batches of 32 samples
    x_test, y_test = np.array([data[0], data[1]]), np.array([1,1])
    model.fit(data, labels, validation_data=(x_test, y_test), epochs=10, batch_size=32)
    scores = model.evaluate(x_test, y_test, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1]*100))

    # y_pred = model.predict(np.array([data[0], data[1]]))
    # print(y_pred)

if __name__ == '__main__':
    model4()

    # model = build(x, y)
    # y_pred = model.predict(x[0])
    # print(y_pred)