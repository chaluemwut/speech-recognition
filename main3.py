import librosa, keras, random
import numpy as np
from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D
from keras.layers import Convolution2D, MaxPooling2D, Convolution1D, Dropout
from keras.utils import np_utils
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score

# https://blog.manash.me/building-a-dead-simple-word-recognition-engine-using-convnet-in-keras-25e72c19c12b

def create_feature():
    x_feature = []
    y = []
    for i in range(31):
        x, rate = librosa.load('data/training/cat/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        x_feature.append(mfcc)
        y.append(1)

    for i in range(31):
        x, rate = librosa.load('data/training/dog/{}.wav'.format(i))
        mfcc = librosa.feature.mfcc(y=x, sr=rate, n_mfcc=40)
        x_feature.append(mfcc)
        y.append(0)

    return np.array(x_feature)

def wav2mfcc(file_path):
    wave, sr = librosa.load(file_path, mono=True, sr=None)
    wave = wave[::3]
    mfcc = librosa.feature.mfcc(wave, sr=16000)
    return np.array(mfcc)

def process():
    x_train = np.random.random((3,3,3))
    y_train = np.random.randint(2, size=(3, 1))
    model = Sequential()
    model.add(Conv1D(filters=32, kernel_size=2, activation='relu', input_shape=(3,3)))
    model.add(Conv1D(filters=32, kernel_size=2, activation='relu'))
    model.add(MaxPooling1D(1))
    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop')
    model.fit(x_train,y_train)
    y_pred = model.predict(np.random.random(3,3,1))
    print(y_pred)


create_feature()